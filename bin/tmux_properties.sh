#!/bin/sh
# Environment substitution to allow for templating .tmux.conf files.
# Useful for e.g. configuring .tmux.conf colours based on hostname
# To be sourced once at login (to get around situations where $HOME is synched
# between hosts)
# Add
#    source $0 [path to template file] [path to your .tmux.conf] || true
# to your $HOME/.profile

if [ "$#" -lt "2" ]; then
	echo "Usage: $0 [template file] [tmux configuration file]"
	exit 1;
fi

# We only need to do this once per login session at worst
if [ -z "$TMUX_CONFIG_TEMPLATE" ]; then
	(

	tmpdir=$(mktemp -d "${TMPDIR:-/tmp}/tmux_templating.XXXXXX")
	vars_b="${tmpdir}/vars.before"
	vars_a="${tmpdir}/vars.after"
	# Accent colour mappings
	COLOURS=""
	colour=""
	I=0

	# Fetch variables for filtering of local ones set below
	(set) | sed -e 's/=.*$//'  > "${vars_b}"
	# Colours
	#SOLARIZED HEX         16/8 TERMCOL  XTERM/HEX   L*A*B      RGB         HSB

	#Background (dark)
	COLOUR_b03="#002b36"  # 8/4 brblack  234 #1c1c1c 15 -12 -12   0  43  54 193 100  21
	COLOUR_b02="#073642"  # 0/4 black    235 #262626 20 -12 -12   7  54  66 192  90  26
	#Content
	COLOUR_b01="#586e75"  #10/7 brgreen  240 #585858 45 -07 -07  88 110 117 194  25  46
	COLOUR_b00="#657b83"  #11/7 bryellow 241 #626262 50 -07 -07 101 123 131 195  23  51
	COLOUR_b0="#839496"   #12/6 brblue   244 #808080 60 -06 -03 131 148 150 186  13  59
	COLOUR_b1="#93a1a1"   #14/4 brcyan   245 #8a8a8a 65 -05 -02 147 161 161 180   9  63
	#Background (light)
	COLOUR_b2="#eee8d5"   # 7/7 white    254 #e4e4e4 92 -00  10 238 232 213  44  11  93
	COLOUR_b3="#fdf6e3"   #15/7 brwhite  230 #ffffd7 97  00  10 253 246 227  44  10  99

	#Accent
	COLOUR_y="#b58900"    # 3/3 yellow   136 #af8700 60  10  65 181 137   0  45 100  71
	COLOUR_o="#cb4b16"    # 9/3 brred    166 #d75f00 50  50  55 203  75  22  18  89  80
	COLOUR_r="#dc322f"    # 1/1 red      160 #d70000 50  65  45 220  50  47   1  79  86
	COLOUR_m="#d33682"    # 5/5 magenta  125 #af005f 50  65 -05 211  54 130 331  74  83
	COLOUR_v="#6c71c4"    #13/5 brmagenta 61 #5f5faf 50  15 -45 108 113 196 237  45  77
	COLOUR_b="#268bd2"    # 4/4 blue      33 #0087ff 55 -10 -45  38 139 210 205  82  82
	COLOUR_c="#2aa198"    # 6/6 cyan      37 #00afaf 60 -35 -05  42 161 152 175  74  63
	COLOUR_g="#859900"    # 2/2 green     64 #5f8700 60 -20  65 133 153   0  68 100  60

	# Mappings (dark mode)
	COLOUR_BG=$COLOUR_b03      #Background
	COLOUR_BG_HL=$COLOUR_b02   #Background highlights
	COLOUR_C2=$COLOUR_b01      #Secondary content
	COLOUR_C=$COLOUR_b0        #Primary content
	COLOUR_CE=$COLOUR_b1       #Emphasized content
	COLOUR_CA=$COLOUR_b00      #Alternative content colour
	COLOUR_BG_I=$COLOUR_b3     #Background inverted
	COLOUR_BG_I_HL=$COLOUR_b2  #Background inverted highlights

	# Accent colour mappings
	COLOURS="y o r m v b c g"
	I=0
	for colour in $(hostname | openssl dgst -binary -sha256 | shuf -e $COLOURS --random-source=/dev/stdin); do
		eval "COLOUR_A_$I"='$'COLOUR_$colour
		I=`expr $I + 1`
	done

	# Fetch variables again, local variables will be listed as well now
	(set) | sed -e 's/=.*$//'  > "${vars_a}"

	# Filter so that only the local variables are left
	# Of lines only in before, both, and only in after we choose those only in after:
	vars=$(comm -13 --nocheck-order "${vars_b}" "${vars_a}" | sed -e 's/\(.*\)/$\1/')

	# envsubst needs variables to be environment variables
	# export in subshell so that they don't persist
	(
	for var in $vars; do
		export $(echo "${var}" | sed -e 's/^\$//')
	done

	cat <<EOF
#####################################################################
## tmux configuration file created from template with placeholders ##
## Template: ${1}
#####################################################################
## Variables:
EOF
	for v in $vars; do
		echo "##   $v: $(eval "echo $v")"
	done
	cat <<EOF
#####################################################################

EOF
	envsubst "${vars}" < "$1"
	) > "$2"

	TMUX_CONFIG_TEMPLATE='set'
	)

fi
# temp files cleanup
trap "rm -rf $tmpdir" EXIT
